import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
public class TestClass {

    /**
     * Write your tests here.
     * Don't forget to put @Test before your test method.
     * */

    @Test
    public void test() throws IOException {
        Adder a = new Adder(2,3);
        assertEquals(4, a.add());
    }

}
